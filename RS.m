function [ H, F, R2 ] = RS( data, scale, varargin )
%RS Rescaled Range analysis for a dataset
%
%   Hq = RS (data, scale) calculates the Hurst exponent
%       for data using rescaled range analysis; scale is a row vector given in samples
%   Hq = RS (data, scale, integrate)
%       integrates the signal before running RS; allowed = -1 (differentiate), 0, 1;
%       default = 0
%   [Hq, Fq] = RS (...) returns also the fluctuation function as vector
%   [Hq, Fq, R2] = RS (...) returns also the R2 value of the linear fit in
%       the log-log plot of scale versus fluctuation function. Note that R2 is 
%       very sensitive to errors if the slope is close to zero.

%   Written by Bertrand Kerres, kerres@kth.se, last update 2016-12-05
%
%   This program was written during as part of the research in the
%   Competence Center for Gas Exchange (CCGEx) at KTH Royal Institute of
%   Technology. It was originally developed for the investigation described
%   here: Kerres, B. and Nair, V. and Cronhjort, A. and Mihaescu, M.,
%   Analysis of the Turbocharger Compressor Surge Margin Using a
%   Hurst-Exponent-based Criterion, SAE Int. J. Engines 9(3), 2016,
%   doi:10.4271/2016-01-1027. Please acknowledge this if you use this
%   programme for your own research.
%   
%   Copyright (C) 2016 Bertrand Kerres
%
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   (http://www.gnu.org/licenses/)

    
    ip = inputParser ();
    ip.addRequired ('data', @(x) isvector(x) && isnumeric(x));
    ip.addRequired ('scale', @(x) all(x>0) && size(x,1) == 1);
    ip.addOptional ('integrate', 0, @(x) isscalar(x) && (x==-1 || x==0 || x==1));
    ip.parse (data, scale, varargin{:});
    
    data = data - mean (data);
    if isrow (data)
        data = data';
    end

    scale = ip.Results.scale;   % Scale vector

    % Change global profile function by integrating or differentiating
    if ip.Results.integrate == 1
        data = cumsum (data);
    elseif ip.Results.integrate == -1
        data = [0; diff(data)];
        data = data - mean(data);
    end

    N = length (data);
    F = zeros (size(scale));

    sc = 1;
    
    for s = scale
        Ns = floor (N/s);   % Number of segments
        RS = zeros (Ns, 1);
        
        % Loop through data and get endpoint position
        for v = 1 : Ns
            xv1 = data((v-1)*s+1:v*s);

            % Rescaled range analysis of the local profile function
            % Range determined as difference between max and min
            % Rescaled by the standard deviation
            Yv1 = cumsum (xv1 - mean(xv1));
            RS(v) = (max(Yv1) - min(Yv1)) / std(xv1, 1);
        end
        
        % Calculate root mean of the window variances
        F(sc) = mean (RS);
        sc = sc + 1;
    end
    
    % Fit linear in the log-log plot of time scale vs fluctuation function
    % --> Hurst exponent
    logF = log(F);
    [C, S] = polyfit (log(scale), logF, 1);
    H = C(1) - ip.Results.integrate;
    SS_tot = sum( (logF - mean(logF)).^2 );
    R2 = 1 - S.normr^2 / SS_tot;
end