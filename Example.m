%% Setup
fs = 10e3;  % Sampling frequency [Hz]
T = 10;     % Sampling duration [s]

x = normrnd(0,1, T*fs, 1);  % Test signal (here white noise)
q = 2; %(-3:1:3)';          % Hurst exponent orders
m = 1;                      % Detrending order 

% Scales are 24 logarithmically spaced values from 10 samples to 1/8 of the sampling
% duration, given in numbers of samples
ts_min = 10/fs;     % Min timescale in samples
ts_max = T/8;       % Max timescale in samples
no_ts = 24;
ep = linspace(log2(fs*ts_min), log2(fs*ts_max), no_ts);
scale = round (2.^ep);

%% Calculation
[H, F] = MFDFA(x, scale, q, m);

%% Timescale - structure function plot
figure();
hAx = axes ();
hold (hAx, 'on');
grid (hAx, 'on');

hP = plot (hAx, log2(scale/fs), log2(F), 'xk');

xlabel ('log_2 (t_s [sec])');
ylabel ('log_2 (F^2)');
